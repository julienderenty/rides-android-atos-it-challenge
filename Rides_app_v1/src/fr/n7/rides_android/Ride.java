package fr.n7.rides_android;

import java.util.ArrayList;

import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.util.GeoPoint;

public class Ride {

	public int id;
	public String titre;
	public ArrayList<GeoPoint> waypoints=new ArrayList<GeoPoint>();
	public Road road;
	public int nbPlaces=0;
	public int volumeCoffre=0;


	public Ride(String string) {
		titre=string;
	}


	public String toString(){
		return titre;
	}

}
