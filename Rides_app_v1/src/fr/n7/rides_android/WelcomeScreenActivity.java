package fr.n7.rides_android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;

import fr.n7.rides_android.account.AccountActivity;
import fr.n7.rides_android.account.ProfileActivity;

public class WelcomeScreenActivity extends RidesActivity {
	TextView profile;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ecran_accueil);

		profile = (TextView) findViewById(R.id.name);

		((Button) findViewById(R.id.buttonPassager)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(WelcomeScreenActivity.this,fr.n7.rides_android.passenger.DescribingRideActivity.class);
				startActivityForResult(intent, 0);
			}
		});

		((Button) findViewById(R.id.buttonConducteur)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(WelcomeScreenActivity.this,fr.n7.rides_android.driver.RideDescribingActivity.class);
				startActivityForResult(intent, 0);
			}
		});


		((Button) findViewById(R.id.buttonProfile)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				Intent intent = new Intent(WelcomeScreenActivity.this,ProfileActivity.class);
				startActivity(intent);
			}
		});

		((Button) findViewById(R.id.buttonDeco)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				facebookDeco();

				Editor edit = getSharedPreferences(AccountActivity.FILENAME,MODE_PRIVATE).edit();
				edit.remove(AccountActivity.USERNAME);
				edit.remove(AccountActivity.PASSWORD);
				edit.commit();

				Intent intent = new Intent(WelcomeScreenActivity.this,AccountActivity.class);
				WelcomeScreenActivity.this.startActivity(intent);
				((Activity) WelcomeScreenActivity.this).finish();
			}
		});

		facebookIni();
	}


	/********************FACEBOK CODE***************************/
	private ProfilePictureView profilePictureView;

	private void facebookIni(){

		if(Session.getActiveSession()!=null && Session.getActiveSession().isOpened()){

			// make request to the /me API
			Request.executeMeRequestAsync(Session.getActiveSession(), new Request.GraphUserCallback() {

				// callback after Graph API response with user object
				@Override
				public void onCompleted(GraphUser user, Response response) {
					profilePictureView = (ProfilePictureView) findViewById(R.id.profilePicture);
					if (user!=null){
						profilePictureView.setProfileId(user.getId());
						profile.setText(user.getFirstName());
					}
				}
			});
		}
	}

	private void facebookDeco() {
		if(Session.getActiveSession()!=null){
			Session.getActiveSession().close();
		}
	}
}


