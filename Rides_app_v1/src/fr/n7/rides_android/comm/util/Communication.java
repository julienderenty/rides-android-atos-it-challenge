package fr.n7.rides_android.comm.util;

import org.json.JSONObject;

import android.R.bool;
import android.app.Activity;
import android.app.Service;
import android.widget.TextView;

public interface Communication {

	    
	    
	    public String sendMessageRef(String id, String message, Activity a);
	    public boolean isNetworkAvailable(Activity a);
	    public boolean isNetworkAvailable(Service a);
	    
	    public boolean haveNetworkConnection(Activity a);
	    
	    public boolean startConnection(Activity a, String id);
	    public void finishConnection();
	    
	    public void startService();
		public boolean sendMessage(String id, String string, Service remoteService);
}
