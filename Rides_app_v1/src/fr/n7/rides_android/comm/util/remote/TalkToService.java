package fr.n7.rides_android.comm.util.remote;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import fr.n7.rides_android.comm.util.remote.TalkToServiceImpl.ProfileSummary;

public interface TalkToService  {
	public void sendMessage(PTCL e, String msg);
	public void initConnection(Activity e);
	public void stopConnection(Activity e);


	/**
	 * Http Post
	 * @param login
	 * @param code
	 * @return 
	 */
	public List<ProfileSummary> sendLogin(String login, String code);


	/**
	 * JSON type : askForRide
	 * @param askeruid
	 * @param lat
	 * @param lon
	 */
	public void sendAskForRide(long offereruid, int lat, int lon);


	/**
	 * JSON 
	 * @param geoPtSrc
	 * @param geoPtDest
	 * @param seatsNb
	 * @param waypoints
	 */
	public void sendStoreRide(int originLat, int originLon, int destLat, int destLon, int seatsNb, 
			Date arrivalDate,
			List<TalkToServiceImpl.latlon> waypoints);


	/**
	 * JSON
	 * Position reguliere
	 */
	public void sendUpdatePosition(int lat, int lon) ;


	/**
	 * JSON 
	 * O refuse
	 * 1 accepter
	 * 2 refus timeout
	 */
	public void sendChoiceFromDriver(int choiceFromDriver, long askeruid);










	/**
	 * HTTP
	 * Response:

Json array of:

	distanceToRide:		Int
	distanceToDestination:	Int
	arrivalDate:		Long
	offererId:		Long
	offererName:		String
	offererUsername:	String
	offererScore:		Int
	offererProfilePicURL:	String
	destLat:		Int
	destLon:		Int
	originLat:		Int
	originLon:		Int
	nbReviews:		Int
	waypoints:		Json array of:

					lat:	Int
					lon: 	Int
	 * @param geoPtSrc
	 * @param geoPtDest
	 * @param passagersNb
	 */

	public void sendSearchMessage(int originlat, int originlon, int destlat, int destlon, int nbpersonnes);

	/**
	 * HTTP GET
	 * @param userId
	 * returns Profil
	 */
	public void sendGetProfile(int userId);

	public void signUp(String username, String mail, String firstname,
			String phonenumber, String pass);

	public void logOut();
}
