package fr.n7.rides_android.comm.util.remote;

import java.util.List;

public interface MessageReceivedProcess {
	/*
	 * copied
	 */
	public class locationUpdate {
		String userId;
        int lat;
        int lon;
	 }
	/**
	 * 
	 * @param a
	 */
     public void traiterLocationUpdate(locationUpdate a);
   
    public class newAskerNotification {
         String askerId;// -> JsString(askerId.toString),
         String askerName; //" -> JsString(asker.firstname),
         String askerUsername; //" -> JsString(asker.username),
         String askerScore; //" -> JsString(asker.score.toString),         
         String askerPhoneNumber;//" -> JsString(asker.phonenumber),
         String askerProfilePicURL;//" -> JsString(asker.avatar),
         int lat;//" -> JsString(askerLat.toString),
         int lon;//" -> JsString(askerLon.toString)
     }
     /*
      * copied
      * message received by the driver 
      * when the passenger makes a request
      */
     public void traiterAskerNotification(newAskerNotification a);
     
     
     public class acceptedRideNotification {
                 //String type; //-> JsString("acceptedRideNotification"),
                 long offererId; //-> JsString(offererId.toString),
                 int originLat; //-> JsString(ride._2.toString),
                 int originLon; //" -> JsString(ride._3.toString),
                 int destLat; //" -> JsString(ride._4.toString),
                 int destLon; //" -> JsString(ride._5.toString),
                 long arrivalDate; // -> JsString(ride._6.toString),
                 List<int[]>waypoints ;//" -> JsArray(
//                             "lat" -> JsString(p._1.toString),
//                             "lon" -> JsString(p._2.toString)
     }
     /**
      * The driver accepts sharing and passenger receives message.
      * @param a
      */
     public void traiterAcceptedRideNotification(acceptedRideNotification a);
     
     
     public class refusedRideNotification {
        long offererId;// -> JsString(offererId.toString)
     }
     /**
      * The driver refuse sharing and passenger receives message.
      * @param a
      */
     public void traiterRefusedRideNotification(refusedRideNotification a);
}
