package fr.n7.rides_android.comm.util.remote;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.Log;

import com.google.gson.Gson;

public class TalkToServiceImpl  extends AsyncTask<String, Void, String> implements TalkToService {
	static String url = "http://ec2-54-242-174-211.compute-1.amazonaws.com";
	Gson a;
	Messenger myService = null;
	public boolean isBound;
	public ReentrantLock l;
	public static HttpClient httpclient = new DefaultHttpClient();
    public TalkToServiceImpl(){
    	super();
    	a  = new Gson();

    }
	private ServiceConnection myConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			myService = new Messenger(service);
			isBound = true;
			Log.v("TalkToSevice", "started");
		}

		public void onServiceDisconnected(ComponentName className) {
			myService = null;
			isBound = false;
			Log.v("TalkToService", "stopped");
		}
	};

	@Override
	public void sendMessage(PTCL ei, String msge) {
		// TODO Auto-generated method stub
		if (!isBound) {
			Log.v("TTSIMPL","service not bounded while sending message");
		}else{ 
			Log.v("TTSIMPL", "service bounded while sending message");
			Message msg = Message.obtain();
			Bundle bundle = new Bundle();
			try {
				ATS a = (ATS) ei;
				bundle.putString(a.name(), msge);
			} catch (Exception e) {
				try {
					WBSTS a = (WBSTS) ei;
					bundle.putString(a.name(), msge);
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
			msg.setData(bundle);
			try {
				myService.send(msg);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}


	public Object sendMessageHttp(String msge){
		try {
			String URL = "";
			HttpResponse response = httpclient.execute(new HttpGet(URL));
			StatusLine statusLine = response.getStatusLine();
			if(statusLine.getStatusCode() == HttpStatus.SC_OK){
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				String responseString = out.toString();

				//..more logic
			} else{
				//Closes the connection.
				response.getEntity().getContent().close();
				throw new IOException(statusLine.getReasonPhrase());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public TalkToServiceImpl(Service e){
		super();
		Intent intent = new Intent("com.util.remote.RemoteService");
		e.bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
    	a  = new Gson();
	}

	public TalkToServiceImpl(Activity aa){
		super();
		Intent intent = new Intent("com.util.remote.RemoteService");
		aa.bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
    	a  = new Gson();
	}
	

	@Override
	public void initConnection(Activity e) {
		Intent intent = new Intent("com.util.remote.RemoteService");
		e.bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	public void stopConnection(Activity e) {
		e.unbindService(myConnection);
	}
     
	
	
    public class ProfileSummary {
    	public ProfileSummary(){
    		super();
    	}
    }
	/**
	 * Http Post
	 * Login returns session
	 * 
	 * @param login
	 * @param code
	 */
	@SuppressLint("NewApi")
	public List<ProfileSummary> sendLogin(String login, String code) {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		String ur = url+"/logging";
		String aa= doInBackground("login","POST",ur, "username", login,"pass", code);
		if(aa != null | !aa.equals("fail"))
		   sendMessage(ATS.ATS_connect, aa);
		return null;
	}

/*
 * copied
 */
	class AskForRide {
		String type = "askForRide";
		long offereruid;
		int lat; 
		int lon;
		public AskForRide(long offereruid, int lat, int lon) {
			super();
			this.offereruid = offereruid;
			this.lat = lat;
			this.lon = lon;
		}
		public AskForRide(){
			super();
		}
	}




	/**
	 * JSON type : askForRide
	 * @param askeruid
	 * @param lat
	 * @param lon
	 */
	public void sendAskForRide(long offereruid, int lat, int lon){
		AskForRide aa = new AskForRide(offereruid, lat, lon);
		String ar = a.toJson(aa);
		sendMessage(ATS.ATS_message, ar);
		System.out.println(ar);
	}
	public class latlon {
		int lat;
		int lon;
		public latlon(int lat, int lon) {
			super();
			this.lat = lat;
			this.lon = lon;
		}
		public latlon() {
			super();
		}    	
	}
	/*
	 * copied
	 */
	class StoreRide {
		String type="storeRide";
		int originLat;
		int originLon;
		int destLat; 
		int destLon;
		int nbPlaces;
		Long arrivalDate;
		List<latlon> waypoints;
		public StoreRide() {
			super();
		}
		public StoreRide(int originLat, int originLon, int destLat,
				int destLon, int seatsNb, Long arrivalDate,
				List<latlon> waypoints) {
			super();
			this.originLat = originLat;
			this.originLon = originLon;
			this.destLat = destLat;
			this.destLon = destLon;
			this.nbPlaces = seatsNb;
			this.arrivalDate = arrivalDate;
			this.waypoints = waypoints;
		}

	}

	/**
	 * JSON 
	 * @param geoPtSrc
	 * @param geoPtDest
	 * @param seatsNb
	 * @param waypoints
	 */
	public void sendStoreRide(int originLat, int originLon, int destLat, int destLon, int nbPlaces, 
			Date arrivalDate,
			List<latlon> waypoints) {
		Calendar aaa = new GregorianCalendar(); 
		aaa.setTime(arrivalDate);

		StoreRide aa = new StoreRide(originLat, originLon, destLat, destLon, nbPlaces, aaa.getTimeInMillis(), waypoints);
		String ar = a.toJson(aa);
		sendMessage(ATS.ATS_message, ar);
		System.out.println(ar);
	}

	/*
	 * copied
	 */
	class UpdatePosition {
		String type = "updatePosition";
		int lat;
		int lon;
		public UpdatePosition(){
			super();
		}
		public UpdatePosition(int lat, int lon) {
			super();
			this.lat = lat;
			this.lon = lon;
		}
	}
	/**
	 * JSON
	 * Position reguliere
	 */
	public void sendUpdatePosition(int lat, int lon) {
		UpdatePosition aa = new UpdatePosition(lat, lon);
		String ar = a.toJson(aa);
		sendMessage(ATS.ATS_message, ar);
	}
/*
 * copied
 */
	class choiceFromDriver{
		String type = "choiceFromDriver";
		int choiceFromDriver;
		long askeruid;
		public choiceFromDriver() {
			super();
		}
		public choiceFromDriver(int choiceFromDriver, long askeruid) {
			super();
			this.choiceFromDriver = choiceFromDriver;
			this.askeruid = askeruid;
		}
	}
	/**
	 * JSON 
	 * O refuse
	 * 1 accepter
	 * 2 refus timeout
	 */
	public void sendChoiceFromDriver(int choiceFromDriver, long askeruid) {
		choiceFromDriver aa = new choiceFromDriver(choiceFromDriver, askeruid);
		String ar = a.toJson(aa);
		sendMessage(ATS.ATS_message, ar);
	}

	/**
	 * HTTP POST
	 * retourne session si signup reussi
	 */
	@SuppressLint("NewApi")
	@Override
	public void signUp(String username,String mail, String firstname, String phonenumber, String pass){
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		String ur = url+"/registering";
		String res = doInBackground("signup","POST", ur,"username",username, "mail", mail,
				"firstname", firstname, "phonenumber", phonenumber, 
				"pass", pass);	
		if(res != null | !res.equals("fail"))
			  sendMessage(ATS.ATS_connect, res);
	}

	/**
	 * HTTP GET
	 */
	@SuppressLint("NewApi")
	public void logOut(){
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		String ur = url+"/logout";
		doInBackground("logout", "GET",ur);
	}





	/**
	 * HTTP
	 * @param geoPtSrc
	 * @param geoPtDest
	 * @param passagersNb
	 */
	public void sendSearchMessage(int originlat, int originlon, int destlat, int destlon, int nbpersonnes) {

	}

	public String getSession(HttpResponse response){
		String	sessionID = response.getFirstHeader("Set-Cookie").getValue();
		Log.i("TTSIMPL - http response", sessionID);	
		try {
			response.getEntity().consumeContent();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sessionID.split(";")[0];
	}

	//TODO adapter pour pouvoir tirer la session
	@Override
	protected String doInBackground(String... arg0) {
		String res = "";
		try {
			if(arg0[1].equals("POST")){
				String a = arg0[2];
				URL url= new URL(a);

				HttpPost httppost = new HttpPost(url.toString());
				List<NameValuePair> nameValuePairs
				= new ArrayList<NameValuePair>(2);  

				for(int i=3; i < arg0.length; i=i+2){
					Log.v("TTSIMPL - HTTP POST", arg0[i] + "_"+ arg0[i+1] +"_ "+i+" arg.length "+arg0.length);
					nameValuePairs.add(new BasicNameValuePair( arg0[i] , arg0[i+1]));  
				}

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs)); 

				if(arg0[0].equals("login") || arg0[0].equals("signup")){
					HttpResponse response = httpclient.execute(httppost);
					if(response.getStatusLine().getReasonPhrase().equals("OK"))
					    res = getSession(response);
					else
						res = "fail";
					Log.v("TTSIMPL HTTP POST", res);
//					ResponseHandler<String> responseHandler=new BasicResponseHandler();
//					String responseBody = httpclient.execute(httppost, responseHandler);
//					Log.v("TTSIMPL HTTP POST", responseBody);

				}else{
					ResponseHandler<String> responseHandler=new BasicResponseHandler();
					String responseBody = httpclient.execute(httppost, responseHandler);
					Log.v("TTSIMPL HTTP POST", responseBody);
				}
			}else{
				String a = arg0[2];
				URL url= new URL(a);
			    HttpGet httpget = new HttpGet(url.toString());
			    List<NameValuePair> nameValuePairs
				= new ArrayList<NameValuePair>(2);  

				for(int i=3; i < arg0.length; i=i+2){
					Log.v("TTSIMPL - HTTP GET", arg0[i] + "_"+ arg0[i+1]);
					httpget.getParams().setParameter(arg0[i], arg0[i+1]);
				}
				ResponseHandler<String> responseHandler=new BasicResponseHandler();
				String responseBody = httpclient.execute(httpget, responseHandler);
				Log.v("TTSIMPL HTTP GET", responseBody);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}
	
	@Override
	public void sendGetProfile(int userId) {
		// TODO Auto-generated method stub
		
	}
}
