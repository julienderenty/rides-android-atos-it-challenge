package fr.n7.rides_android.comm.util.remote;


import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.widget.Toast;
import fr.n7.rides_android.comm.util.CommunicationWebSocket;
import fr.n7.rides_android.comm.util.ListenerWebSock;
import fr.n7.rides_android.comm.util.WebSocketClient;

public class RemoteService extends Service {
	
	
    final Messenger myMessenger = new Messenger(new IncomingHandler());
	static public WebSocketClient client = null; 
	static public Handler handler = new Handler();
	static final String ACTION = "RemoteAction";
	static final Intent aa = new Intent(ACTION);
	static boolean onRealServer = true;

	class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
        	chooser(msg);
        }
    }
	
	public void connect(String session){
		String urlChat = "";
		if(!onRealServer)
		  urlChat= "ws://ec2-184-72-134-63.compute-1.amazonaws.com/room/chat?username="+session;
		else
		  urlChat ="ws://ec2-54-242-174-211.compute-1.amazonaws.com/connect";
		
		if(isNetworkAvailable()){
			try {
				List<BasicNameValuePair> extraHeaders = Arrays.asList(
						new BasicNameValuePair("Cookie", session)
						);
				client = new WebSocketClient(URI.create(urlChat), new ListenerWebSock(this),extraHeaders);
				client.connect();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager 
		= (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}
	
	
	public boolean sendMessage(String id, String msg){
		if(isNetworkAvailable()){
			try {
				client.send(msg);
				return true;
			} catch (Exception e) {
				return false;
			}
		}else{
			return false;
		}
	}
	
	public void chooser(Message msg){
    	Bundle a = msg.getData();
		if(a.containsKey(ATS.ATS_connect.name()))
			connect(a.getString(ATS.ATS_connect.name()));
		if(a.containsKey(WBSTS.WBSTS_connected.name())){
			showMessage(a.getString(WBSTS.WBSTS_connected.name()));
	    	aa.putExtra("message", a.getString(WBSTS.WBSTS_connected.name()));
	    	sendBroadcast(aa);
		}
		if(a.containsKey(WBSTS.WBSTS_onMessage.name())){
			showMessage(a.getString(WBSTS.WBSTS_onMessage.name()));
			aa.putExtra("message", a.getString(WBSTS.WBSTS_onMessage.name()));
			sendBroadcast(aa);
		}
		if(a.containsKey(a.getString(WBSTS.WBSTS_onError.name()))){
			showMessage(a.getString(WBSTS.WBSTS_onError.name()));
			aa.putExtra("message", a.getString(WBSTS.WBSTS_onError.name()));
			sendBroadcast(aa);
		}
		if(a.containsKey(ATS.ATS_message.name())){
			sendMessage("Tablet", a.getString(ATS.ATS_message.name()));
		}
		
	}
	private void showMessage(String string) {
		Toast.makeText(getApplicationContext(), 
                string, Toast.LENGTH_SHORT).show();
	}
	@Override
	public IBinder onBind(Intent intent) {
		return myMessenger.getBinder();
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		int flags = 0;
		super.onStartCommand(intent, flags, startId);
		Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void onDestroy() {
		Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
		CommunicationWebSocket.client.disconnect();
		super.onDestroy();
	}
	
	
	public static void setIntent(String a ){
		aa.setAction(a);
	}
}