package fr.n7.rides_android.comm.util.remote;

import com.google.gson.Gson;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ServiceReceiver extends BroadcastReceiver {
	MessageReceivedProcess msgr ;
	
	@Override
	public void onReceive(Context context, Intent intent) {
    	String message = intent.getStringExtra("message");
    	//received.setText(message);
        class Mess {
        	public String type;
        	public Mess(String t){
        		type = type;
        	}
        }
        Gson b = new Gson();	
    	Mess msg = b.fromJson(message, Mess.class);
    	if (msg.type.equals( "locationUpdate")) {	
			msgr.traiterLocationUpdate(b.fromJson(message, MessageReceivedProcess.locationUpdate.class));
    	}
    	if (msg.type.equals( "newAskerNotification")) {	
    		msgr.traiterAskerNotification(b.fromJson(message, MessageReceivedProcess.newAskerNotification.class));
    	}
    	if (msg.type.equals( "acceptedRideNotification")) {	
    		msgr.traiterAcceptedRideNotification(b.fromJson(message, MessageReceivedProcess.acceptedRideNotification.class));
    	}
    	if (msg.type.equals( "refusedRideNotification")) {	
    		msgr.traiterRefusedRideNotification(b.fromJson(message, MessageReceivedProcess.refusedRideNotification.class));
    	}	
    	
	}
	
	public ServiceReceiver(MessageReceivedProcess a){
	    msgr = a ;
	}
}
