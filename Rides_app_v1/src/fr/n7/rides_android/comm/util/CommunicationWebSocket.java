package fr.n7.rides_android.comm.util;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;



public class CommunicationWebSocket implements Communication  {
	final public static String url= "ws://ec2-54-242-174-211.compute-1.amazonaws.com/connect?terminalId=";
	final public static String urlChat= "ws://ec2-184-72-134-63.compute-1.amazonaws.com/room/chat?username=";

	static public WebSocketClient client = null; 




public boolean isNetworkAvailable(Activity a) {
		ConnectivityManager connectivityManager 
		= (ConnectivityManager) a.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}
	
	public boolean isNetworkAvailable(Service a) {
		ConnectivityManager connectivityManager 
		= (ConnectivityManager) a.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	public boolean haveNetworkConnection(Activity a) {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) a.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}

	@Override
	public boolean startConnection(Activity ab,String terminalId) {
		if(isNetworkAvailable(ab)){
			try {
				client.connect();
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}

		}else
			return false;
	}
	
	
	@Override
	public void finishConnection() {
		if(client != null)
			client.disconnect();
	}


	@Override
	public boolean sendMessage(String id, String string, Service r) {
		boolean a = isNetworkAvailable(r);
		if(isNetworkAvailable(r)){
			try {
				client.connect();
			} catch (Exception e) {
				a = false;
			}
			try {
				client.send(string);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return a;
	}

	@Override
	public String sendMessageRef(String id, String message, Activity a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void startService() {
		// TODO Auto-generated method stub
		
	}
	
	
}

