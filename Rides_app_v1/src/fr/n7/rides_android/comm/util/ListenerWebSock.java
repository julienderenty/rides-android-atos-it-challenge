package fr.n7.rides_android.comm.util;

import android.app.Service;
import android.os.Message;
import android.util.Log;
import fr.n7.rides_android.comm.util.remote.TalkToService;
import fr.n7.rides_android.comm.util.remote.TalkToServiceImpl;
import fr.n7.rides_android.comm.util.remote.WBSTS;

public class ListenerWebSock implements WebSocketClient.Listener {

	
	private TalkToService a;
	@Override
	public void onConnect() {
		Log.d("WebSocket", "Connected!");
		a.sendMessage(WBSTS.WBSTS_connected, "Connected!");
	}

	@Override
	public void onMessage(String message) {
		Log.d("WebSocket", String.format("Got string message! %s", message));
		//SkeletonActivity.setText(message);
		Message msg = new Message();
		msg.obj = message;
		//SkeletonActivity.mHandler.sendMessage(msg);
		a.sendMessage(WBSTS.WBSTS_onMessage, message);
	}

	@Override
	public void onMessage(byte[] data) {
		Log.d("WebSocket", String.format("Got binary message! %s", data));
		a.sendMessage(WBSTS.WBSTS_onMessage, String.format("Got binary message! %s", data));
	}

	@Override
	public void onDisconnect(int code, String reason) {
		Log.d("WebSocket", String.format("Disconnected! Code: %d Reason: %s", code, reason));
		a.sendMessage(WBSTS.WBSTS_onMessage, String.format("Disconnected! Code: %d Reason: %s", code, reason) );
	}

	@Override
	public void onError(Exception error) {
		Log.e("WebSocket", "Error!", error);
		a.sendMessage(WBSTS.WBSTS_onError, error.getMessage());
	}
	
	public ListenerWebSock(Service e){
		a = new TalkToServiceImpl(e);
	}
}
