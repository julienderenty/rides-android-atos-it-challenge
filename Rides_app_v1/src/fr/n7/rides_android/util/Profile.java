package fr.n7.rides_android.util;

//v0.1 --> validated

import java.util.List;

import android.graphics.ImageFormat;
import android.location.Location;
import android.text.Html.ImageGetter;
import android.widget.ImageView;

public class Profile {
	private String username;
	private String name;
	private Location location;
	private int score;
	private List<Comment> listComment;
	private String interests;
	private String avatarUrl;
	
	public enum AgeRange {
		AR10to20, AR20to30, AR30to40, AR40to50, AR50to60, AR60to70,
		AR70to80, AR80to90, AR90to100, AR100to110, AR110to120;
	}
	private AgeRange ageRange;
	
}
