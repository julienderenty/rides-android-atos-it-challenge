package fr.n7.rides_android.passenger;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import fr.n7.rides_android.R;
import fr.n7.rides_android.Ride;

public class ListAdapterRide extends ArrayAdapter<Ride>{

	public ListAdapterRide(Context context, int textViewResourceId, ArrayList<Ride> m_trajets) {
		super(context, textViewResourceId,m_trajets);
	}



	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) LayoutInflater.from( this.getContext() );
			v = vi.inflate(R.layout.row, null);
		}

		Ride trajet = this.getItem(position);
			if (trajet != null) {
				TextView tt = (TextView) v.findViewById(R.id.headline);
				//TextView bt = (TextView) v.findViewById(R.id.bottomtext);
				if (tt != null) {
					tt.setText("Name: "+trajet.titre);                            }
				//                    if(bt != null){
				//                          bt.setText("Status: "+trajet.nbPlaces);
				//                    }
			}
		return v;
	}

}
