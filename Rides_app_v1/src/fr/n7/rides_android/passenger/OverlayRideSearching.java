package fr.n7.rides_android.passenger;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.OverlayItem;

import android.app.Activity;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import fr.n7.rides_android.driver.DrivingActivity;
import fr.n7.rides_android.driver.RideMakingActivity;
import fr.n7.rides_android.map.ItemizedOverlayDraggable;
import fr.n7.rides_android.map.OverlayRide;

public class OverlayRideSearching extends OverlayRide {
	RideMakingActivity activity;
	protected ItemizedOverlayDraggable overlayDest;

	public OverlayRideSearching(Activity activity, MapView mapView) {
		super(activity, mapView);
		overlayDest=new ItemizedOverlayDraggable(drawable, new DefaultResourceProxyImpl(activity), dragImage,activity, null);
		mapView.getOverlays().add(overlayDest);
	}
	
	@Override
	public boolean onLongPress(MotionEvent e, MapView mapView) {
		GeoPoint nouveauGP = (GeoPoint) mapView.getProjection().fromPixels((int) e.getX(), (int) e.getY()) ;
		ajouterDestination(nouveauGP);
		mapView.invalidate();
		return true;
	}

	@Override
	public void ajouterDestination(GeoPoint nouveauGP) {
		RideArrivalActivity.arrival=nouveauGP;
		overlayDest.clear();
		overlayDest.addOverlay(new OverlayItem("", "", nouveauGP));
	}

	@Override
	protected void draw(Canvas arg0, MapView arg1, boolean arg2) {
	}
}
