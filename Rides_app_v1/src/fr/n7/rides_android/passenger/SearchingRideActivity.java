package fr.n7.rides_android.passenger;


import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import fr.n7.rides_android.R;
import fr.n7.rides_android.Ride;
import fr.n7.rides_android.RidesActivity;

public class SearchingRideActivity extends RidesActivity {
	
	public static Ride trajetClique;
	public static ListAdapterRide aa;
	public static ArrayList<Ride> liste=new ArrayList<Ride>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_resultat_recherche);
		
		//Ajout bouton pr�c�dent
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		ListView list=(ListView)findViewById(android.R.id.list);

		final Context ctx=this;
		
		aa = new ListAdapterRide(ctx, R.layout.row, new ArrayList<Ride>());		
		list.setAdapter(aa);
		
		int originlat = 0;
		int originlon = 0;
		int destlat = 0;
		int destlon = 0;
		int nbpersonnes = 0;
		server.sendSearchMessage(originlat, originlon, destlat, destlon, nbpersonnes);

		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				trajetClique=aa.getItem(arg2);
				Intent i;
				if (Build.VERSION.SDK_INT < 16) {
					i = new Intent(ctx, DisplayRideActivity.class);
				} else{
					i = new Intent(ctx, fr.n7.rides_android.passenger.v16_swipe.RideDetailsActivity.class);//.v16_swipe.TrajetDetailActivity.class);
				}
				 
				ctx.startActivity(i);
			}
		});

	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==-1){
			setResult(-1);
			finish();
		}
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId()==android.R.id.home){
			setResult(-1);
			finish();
		}
		return true;
	}
	

}
