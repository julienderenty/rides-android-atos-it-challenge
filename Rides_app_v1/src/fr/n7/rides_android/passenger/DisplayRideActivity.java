package fr.n7.rides_android.passenger;


import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.MyLocationOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import fr.n7.rides_android.R;
import fr.n7.rides_android.RidesActivity;

public class DisplayRideActivity extends RidesActivity {

	public MapView mapView;
	private ArrayList<OverlayItem> pList;
	private Drawable pDefaultMarker;
	private ItemizedIconOverlay<OverlayItem> o;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_afficher_trajet);
		TextView t=(TextView) findViewById(R.id.textView1);
		t.setText(SearchingRideActivity.trajetClique.titre);

		Button demande= (Button) findViewById(R.id.buttonDemande);
		demande.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				locationOverlay=new MyLocationOverlay(DisplayRideActivity.this, null);
				long offereruid;
				server.sendAskForRide(SearchingRideActivity.trajetClique.id,(int) (locationOverlay.getLastFix().getLatitude()*1E6), (int) (locationOverlay.getLastFix().getLongitude()*1E6));
			}
		});

		
		pList = new ArrayList<OverlayItem>();
		pDefaultMarker=getResources().getDrawable(R.drawable.marker_poi_default);
		o=new ItemizedIconOverlay<OverlayItem>(pList, pDefaultMarker, null, new DefaultResourceProxyImpl(this));
		
		mapView = (MapView) findViewById(R.id.mapview);

		carPositionUpdate(new GeoPoint(43E6,1E6));

		
		Timer timer=new Timer();
		timer.schedule(new TimerTask() {
			int lat=(int) (43*1E6);
			int lon=(int) 1E6;
			@Override
			public void run() {
				carPositionUpdate(new GeoPoint(lat+=1E6, lon));
			}
		}, 1000, 2000);
		
				
		// SLEEP 2 SECONDS HERE ...
		Handler handler = new Handler(); 
		handler.postDelayed(new Runnable() { 
			public void run() { 
			} 
		}, 7000); 

	}


	public void carPositionUpdate(GeoPoint p){
		Log.e("update!","ok");
		o.removeAllItems(true);
		o.addItem(new OverlayItem("", "", p));
		mapView.getOverlays().add(o);
		mapView.invalidate();
	}

}
