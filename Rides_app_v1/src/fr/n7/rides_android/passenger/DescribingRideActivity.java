package fr.n7.rides_android.passenger;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import fr.n7.rides_android.R;
import fr.n7.rides_android.RidesActivity;

public class DescribingRideActivity extends RidesActivity {

	public static int nbPlaces;
	public static int detourPosition;
	public static int detourDest;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Ajout bouton pr�c�dent
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_desc_trajet_passager);
		
		final Context context = this;
		((SeekBar) findViewById(R.id.seekBarNbPlacesAssises)).setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				nbPlaces=seekBar.getProgress();
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
 
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				((TextView) findViewById(R.id.nbPlacesAssises)).setText(seekBar.getProgress()+"");
				
			}
		});
		

		((SeekBar) findViewById(R.id.seekBarDetourMaxPosition)).setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				detourPosition=seekBar.getProgress();;
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				 
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				((TextView) findViewById(R.id.nbDetourMaxPosition)).setText(seekBar.getProgress()+"");				
			}
		});
		
	((SeekBar) findViewById(R.id.seekBarDetourMaxDestination)).setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				detourDest=seekBar.getProgress();
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				((TextView) findViewById(R.id.nbDetourMaxDestination)).setText(seekBar.getProgress()+""); 
				
			}
		});
		
		Button b= (Button) findViewById(R.id.button1);
		b.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context,RideArrivalActivity.class);
				startActivityForResult(intent,0);				
			}
		});
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==-1){
			setResult(-1);
			finish();
		}
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId()==android.R.id.home){
			setResult(-1);
			finish();
		}
		return true;
	}
	

}
