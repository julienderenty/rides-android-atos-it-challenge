package fr.n7.rides_android.passenger;

import org.osmdroid.util.GeoPoint;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.actionbarsherlock.view.MenuItem;

import fr.n7.rides_android.driver.OverlayRideMaking;
import fr.n7.rides_android.map.RideMakingAbstract;

public class RideArrivalActivity extends RideMakingAbstract {


	public static GeoPoint arrival;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		validation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(RideArrivalActivity.arrival==null){
Toast.makeText(RideArrivalActivity.this, "Choose a destination ", Toast.LENGTH_SHORT).show();
				} else{
					startActivityForResult(new Intent(RideArrivalActivity.this,SearchingRideActivity.class), 0);
				}
			}
		});
		overlayWaypoints=new OverlayRideSearching(this,mapView);
		mapView.getOverlays().add(overlayWaypoints);		
	}
	
}