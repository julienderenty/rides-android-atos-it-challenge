package fr.n7.rides_android.passenger.v16_swipe;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;

import com.actionbarsherlock.view.MenuItem;

import fr.n7.rides_android.R;
import fr.n7.rides_android.RidesActivity;
import fr.n7.rides_android.map.DisplayRoadTask;
import fr.n7.rides_android.passenger.SearchingRideActivity;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class RideDetailsActivity extends RidesActivity  {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	CustomViewPager mViewPager;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_trajet);

		// Show the Up button in the action bar.
		getActionBar().setDisplayHomeAsUpEnabled(true);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (CustomViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = new DummySectionFragment();
			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return SearchingRideActivity.liste.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return SearchingRideActivity.liste.get(position).titre;
		}
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	@SuppressLint("ValidFragment")
	public class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";
		private MapView mapView;
		private ItemizedIconOverlay<OverlayItem> o;

		public DummySectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// Create a new TextView and set its text to the fragment's section
			// number argument value.
			View view = inflater.inflate(R.layout.activity_afficher_trajet, null);
			
			ArrayList<OverlayItem> pList = new ArrayList<OverlayItem>();
			Drawable pDefaultMarker = getResources().getDrawable(R.drawable.car);
			o=new ItemizedIconOverlay<OverlayItem>(pList, pDefaultMarker, null, new DefaultResourceProxyImpl(RideDetailsActivity.this));
			
			mapView = (MapView) view.findViewById(R.id.mapview);
			mapView.setOnTouchListener(new OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					mViewPager.setPagingEnabled(event.getAction()==MotionEvent.ACTION_UP);
					return false;
				}
			});

			ArrayList<GeoPoint> wp=new ArrayList<GeoPoint>();
			final int lat=(int) (43*1E6);
			final int lon=(int) 1E6;

			 int lon2=(int) 2E6;
			wp.add(new GeoPoint(lat,lon));
			wp.add(new GeoPoint(lat,lon2));
			
			new DisplayRoadTask(mapView, RideDetailsActivity.this, false, null).execute(wp);
			
			
			Timer timer=new Timer();
			timer.schedule(new TimerTask() {

				int longi=lon;

				@Override
				public void run() {
					new AsyncTask<Void, Void, Void>(){

						@Override
						protected Void doInBackground(Void... params) {
							// TODO Auto-generated method stub
							return null;
						}
						protected void onPostExecute(Void result) {
							carPositionUpdate(new GeoPoint(lat, longi+=1E3));
						};
						
					}.execute();
				}
			}, 0, 200);
			
			return view;
		}
	
	public void carPositionUpdate(GeoPoint p){
		o.removeAllItems(true);
		o.addItem(new OverlayItem("", "", p));
		((MapView) mapView).getOverlays().add(o);

		mapView.invalidate();
	}
	}

	
	
}
