package fr.n7.rides_android.account;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;

import fr.n7.rides_android.R;
import fr.n7.rides_android.RidesActivity;
import fr.n7.rides_android.WelcomeScreenActivity;
import fr.n7.rides_android.comm.util.remote.TalkToServiceImpl;

public class AccountActivity extends RidesActivity {

	public static final String FILENAME="rides_identification";
	public static final String USERNAME="user";
	public static final String PASSWORD="password";
	public static final String SESSIONID="sessionId";
	public static List<fr.n7.rides_android.comm.util.remote.TalkToServiceImpl.ProfileSummary> profile;
	
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		server=new TalkToServiceImpl(this);

		
		setContentView(R.layout.activity_account);

		SharedPreferences prefs=getSharedPreferences(FILENAME,MODE_PRIVATE);

		if(prefs.contains(USERNAME) && prefs.contains(PASSWORD)){

			String login=prefs.getString(USERNAME, "unreachable");
			String code=prefs.getString(PASSWORD, "unreachable");
			
			try {
				profile=server.sendLogin(login, code);

				Intent intent = new Intent(this,WelcomeScreenActivity.class);
				startActivity(intent);
				finish();

			} catch (Exception e1) {
				Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show();
				finish();
				e1.printStackTrace();
			}
		}

		((Button) findViewById(R.id.buttonSenregistrer)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(AccountActivity.this,RegisterActivity.class);
				startActivityForResult(intent, 0);				
			}
		});

		((Button) findViewById(R.id.buttonSidentifier)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(AccountActivity.this,LoginActivity.class);
				startActivityForResult(intent, 0);				
			}
		});

		
		fbInitialisation(savedInstanceState);

	}


	/**********************FACEBOOK CODE*****************************/
	
	
	/**
	 * Called at launch
	 * @param savedInstanceState
	 */
	private void fbInitialisation(Bundle savedInstanceState) {
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);		
	}
	
	/**
	 * Saves and restore the fb session
	 */
	private UiLifecycleHelper uiHelper;

	
	
	/**
	 * Handling session changes
	 */
	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};
	
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		if(session.isOpened()){
			Intent intent = new Intent(this,WelcomeScreenActivity.class);
			this.startActivity(intent);
			((Activity) this).finish();
		}
	}

	
	/**
	 * Lifecycle Session
	 */
    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);

    }
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);

	}
	
    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    

}

