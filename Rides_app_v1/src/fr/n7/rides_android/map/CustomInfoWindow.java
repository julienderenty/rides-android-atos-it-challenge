package fr.n7.rides_android.map;

import org.osmdroid.bonuspack.overlays.DefaultInfoWindow;
import org.osmdroid.views.MapView;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import fr.n7.rides_android.R;

public class CustomInfoWindow extends DefaultInfoWindow {
    public CustomInfoWindow(MapView mapView) {
            super(R.layout.bonuspack_bubble_passager, mapView);
            Button btn = (Button)(mView.findViewById(R.id.infobulle_valider));
            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                        Toast.makeText(v.getContext(), "Button clicked", Toast.LENGTH_LONG).show();
                }
        });
    }
}