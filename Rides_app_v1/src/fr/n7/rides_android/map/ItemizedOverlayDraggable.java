package fr.n7.rides_android.map;

import java.util.ArrayList;

import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IMapView;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.MapView.Projection;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import fr.n7.rides_android.driver.DrivingActivity;
import fr.n7.rides_android.driver.RideMakingActivity;
import fr.n7.rides_android.passenger.RideArrivalActivity;


/**
 * D�fini le marqueur visuel utilis� pour afficher la position de l'util
 * @author J
 *
 */
public class ItemizedOverlayDraggable extends ItemizedOverlay<OverlayItem>{



	private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
	private OverlayItem inDrag=null;
	private ImageView dragImage=null;
	private int xDragImageOffset=0;
	private int yDragImageOffset=0;
	private int xDragTouchOffset=0;
	private int yDragTouchOffset=0;
	private Runnable onUp;

	public ItemizedOverlayDraggable(Drawable defaultMarker, ResourceProxy r,ImageView dragImage, Context ctx, Runnable onUp) {
		super(defaultMarker,r);
		this.ctx=ctx;
		this.onUp=onUp;
		xDragImageOffset=dragImage.getDrawable().getIntrinsicWidth()/2;
		yDragImageOffset=dragImage.getDrawable().getIntrinsicHeight();
		this.dragImage=dragImage;
	}



	public void addOverlay(OverlayItem overlayItem) {
		mOverlays.add(overlayItem);
		populate();
	}



	@Override
	protected OverlayItem createItem(int i) {
		return mOverlays.get(i);
	}

	@Override
	public int size() {
		return mOverlays.size();
	}

	@Override
	protected boolean hitTest(OverlayItem item, Drawable marker, int hitX,
			int hitY) {
		if(RideMakingActivity.flag ){
			if(mOverlays.size()>0	 && mOverlays.get(mOverlays.size()-1).equals(item))
			return true;
		}
		return super.hitTest(item, marker, hitX, hitY);
	}

	final int reelevateY=90;
	private Context ctx;
	@Override
	public boolean onTouchEvent(MotionEvent event, MapView mapView) {
		final int action=event.getAction();
		final int x=(int)event.getX();
		final int y=(int)event.getY();
		boolean result=false;
		
		if (action==MotionEvent.ACTION_DOWN) {
			for (OverlayItem item : mOverlays) {

				Point p=pointFromGeoPoint(item.getPoint(), mapView);
				super.mDefaultMarker.getBounds().offset(2, 2);
				if (p!=null && hitTest(item, super.mDefaultMarker, x-p.x, y-p.y)) {
	
					result=true;
					inDrag=item;
					mOverlays.remove(inDrag);
					populate();

					xDragTouchOffset=0;
					yDragTouchOffset=0;

					setDragImagePosition(p.x, p.y-reelevateY);
					dragImage.setVisibility(View.VISIBLE);

					xDragTouchOffset=x-p.x;
					yDragTouchOffset=y-p.y;


					break;
				}
			}
		}
		else if (action==MotionEvent.ACTION_MOVE && inDrag!=null) {

			setDragImagePosition(x, y-reelevateY);
			result=true;
		}
		else if (action==MotionEvent.ACTION_UP && inDrag!=null) {

			dragImage.setVisibility(View.GONE);

			GeoPoint pt=(GeoPoint) mapView.getProjection().fromPixels(x-xDragTouchOffset,
					y-yDragTouchOffset-reelevateY);
			OverlayItem toDrop=new OverlayItem(inDrag.getSnippet(), inDrag.getTitle(),pt);

			mOverlays.add(toDrop);
			populate();

			if(onUp!=null) {
				//				int pos=DrivingActivity.trajet.waypoints.indexOf(inDrag.getPoint());
				//				DrivingActivity.trajet.waypoints.set(pos,toDrop.getPoint());
				DrivingActivity.trajet.waypoints.remove(inDrag.getPoint());
				DrivingActivity.trajet.waypoints.add(toDrop.getPoint());
				onUp.run();

			}else {
				RideArrivalActivity.arrival=toDrop.getPoint();
			}


			inDrag=null;
			result=true;


			mapView.invalidate();
		}

		return(result || super.onTouchEvent(event, mapView));
	}

	private void setDragImagePosition(int x, int y) {
		RelativeLayout.LayoutParams lp=(RelativeLayout.LayoutParams)dragImage.getLayoutParams();

		lp.setMargins(x-xDragImageOffset-xDragTouchOffset,y-yDragImageOffset-yDragTouchOffset, 0, 0);
		dragImage.setLayoutParams(lp);
	}



	@Override
	public boolean onSnapToItem(int arg0, int arg1, Point arg2, IMapView arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 
	 * @param x  view coord relative to left
	 * @param y  view coord relative to top
	 * @param vw MapView
	 * @return GeoPoint
	 */

	public static GeoPoint geoPointFromScreenCoords(int x, int y, MapView vw){
		if (x < 0 || y < 0 || x > vw.getWidth() || y > vw.getHeight()){
			return null; // coord out of bounds
		}
		// Get the top left GeoPoint
		Projection projection = vw.getProjection();
		GeoPoint geoPointTopLeft = (GeoPoint) projection.fromPixels(0, 0);
		Point topLeftPoint = new Point();
		// Get the top left Point (includes osmdroid offsets)
		projection.toPixels(geoPointTopLeft, topLeftPoint);
		// get the GeoPoint of any point on screen 
		GeoPoint rtnGeoPoint = (GeoPoint) projection.fromPixels(x, y);
		return rtnGeoPoint;
	}

	/**
	 * 
	 * @param gp GeoPoint
	 * @param vw Mapview
	 * @return a 'Point' in screen coords relative to top left
	 */

	private Point pointFromGeoPoint(GeoPoint gp, MapView vw){

		Point rtnPoint = new Point();
		Projection projection = vw.getProjection();
		projection.toPixels(gp, rtnPoint);
		// Get the top left GeoPoint
		GeoPoint geoPointTopLeft = (GeoPoint) projection.fromPixels(0, 0);
		Point topLeftPoint = new Point();
		// Get the top left Point (includes osmdroid offsets)
		projection.toPixels(geoPointTopLeft, topLeftPoint);
		rtnPoint.x-= topLeftPoint.x; // remove offsets
		rtnPoint.y-= topLeftPoint.y;
		if (rtnPoint.x > vw.getWidth() || rtnPoint.y > vw.getHeight() || 
				rtnPoint.x < 0 || rtnPoint.y < 0){
			return null; // gp must be off the screen
		}
		return rtnPoint;
	}

	public void clear(){
		mOverlays.clear();
		super.populate();
	}

}
