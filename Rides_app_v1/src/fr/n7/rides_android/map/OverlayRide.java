package fr.n7.rides_android.map;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import fr.n7.rides_android.R;

public abstract class OverlayRide extends Overlay{
	
	protected Drawable drawable;
	protected ImageView dragImage;
	protected ImageView dragImageWaypoint;

	protected MapView mapView;
	
public DisplayRoadTask task;
	
	

	public OverlayRide(Activity activity, MapView mapView) {
		super(activity);
		this.mapView=mapView;
		drawable=activity.getResources().getDrawable(R.drawable.marker_destination);
		dragImage=(ImageView) activity.findViewById(R.id.drag);
		dragImageWaypoint=(ImageView) activity.findViewById(R.id.dragWaypoint);
		task=new DisplayRoadTask(mapView, activity, false, null);
	}

	public abstract void ajouterDestination(GeoPoint nouveauGP);

}
