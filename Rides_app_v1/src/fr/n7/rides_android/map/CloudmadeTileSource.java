package fr.n7.rides_android.map;

import org.osmdroid.ResourceProxy;
import org.osmdroid.tileprovider.MapTile;
import org.osmdroid.tileprovider.tilesource.IStyledTileSource;
import org.osmdroid.tileprovider.tilesource.OnlineTileSourceBase;
import org.osmdroid.tileprovider.util.CloudmadeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.util.Log;

public class CloudmadeTileSource extends OnlineTileSourceBase implements IStyledTileSource<Integer> {

	private static final Logger logger = LoggerFactory.getLogger(CloudmadeTileSource.class);

	private Integer mStyle = 1;

	public CloudmadeTileSource() {
		super("88424@2x", null, 3, 17, 256,".png",
				"http://a.tile.cloudmade.com/",
				"http://b.tile.cloudmade.com/",
				"http://c.tile.cloudmade.com/");
		
	}

	@Override
	public String pathBase() {
		if (mStyle == null || mStyle <= 1) {
			return mName;
		} else {
			return mName + mStyle;
		}
	}

	@Override
	public String getTileURLString(final MapTile pTile) {
		final String key = CloudmadeUtil.getCloudmadeKey();
		if (key.length() == 0) {
			logger.error("CloudMade key is not set. You should enter it in the manifest and call CloudmadeUtil.retrieveCloudmadeKey()");
		}
		final String token = CloudmadeUtil.getCloudmadeToken();
		String res=getBaseUrl()+""+ key+"/"+ pathBase()+"/"+ getTileSizePixels()+"/"+ pTile.getZoomLevel()+"/"+
				pTile.getX()+"/"+ pTile.getY()+""+ mImageFilenameEnding+"?token="+ token;
		return res;

	}

	@Override
	public void setStyle(final Integer pStyle) {
		mStyle = pStyle;
	}

	@Override
	public void setStyle(final String pStyle) {
		try {
			mStyle = Integer.parseInt(pStyle);
		} catch (final NumberFormatException e) {
			logger.warn("Error setting integer style: " + pStyle);
		}
	}

	@Override
	public Integer getStyle() {
		return mStyle;
	}
}