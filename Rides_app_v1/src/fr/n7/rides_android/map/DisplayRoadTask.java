
package fr.n7.rides_android.map;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.osmdroid.bonuspack.overlays.ExtendedOverlayItem;
import org.osmdroid.bonuspack.overlays.ItemizedOverlayWithBubble;
import org.osmdroid.bonuspack.routing.OSRMRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.bonuspack.routing.RoadNode;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.OverlayItem;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

import fr.n7.rides_android.R;
import fr.n7.rides_android.driver.DrivingActivity;
import fr.n7.rides_android.driver.OverlayRideMaking;

/**
 * Permet la recuperation d'un iti en background puis affichage sur la carte
 * @author J
 *
 */
public class DisplayRoadTask extends AsyncTask<ArrayList<GeoPoint>, Void, Road>{
	MapView mapView;
	Context ctx=null;
	ProgressDialog dialog;
	private boolean navigation;
	private Runnable fin;
	private Overlay roadOverlay;


	/**
	 * Si on utilise le constructeur avec Context, c'est pour afficher infibulles.
	 * @param mapView
	 * @param ctx
	 * @param b 
	 */
	public DisplayRoadTask(MapView mapView, Context ctx, boolean b,ProgressDialog d){
		this.mapView=mapView;
		this.ctx=ctx;
		this.navigation=b;
		dialog=d;
	}
	
	public DisplayRoadTask(MapView mapView, Context ctx, boolean b,ProgressDialog d,Runnable r){
		this.mapView=mapView;
		this.ctx=ctx;
		this.navigation=b;
		dialog=d;
		this.fin=r;
	}

	
	public boolean displayed(){
		return roadOverlay!=null;
	}
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}
	
	
	@Override
	protected Road doInBackground(ArrayList<GeoPoint>... params) {
		RoadManager roadManager = new OSRMRoadManager();
		return roadManager.getRoad(params[0]);
	}




	@Override
	protected void onPostExecute(Road road) {
		super.onPostExecute(road);
//TODO : implement hinting coordinates osrm  and alternate road

		if(road.mNodes.size()>1){
			this.roadOverlay = RoadManager.buildRoadOverlay(road, mapView.getContext());
			mapView.getOverlays().add(this.roadOverlay);
			
			GeoPoint a=road.mNodes.get(0).mLocation;
			GeoPoint b=road.mNodes.get(road.mNodes.size()-1).mLocation;
			BoundingBoxE6 bb;

			if(navigation){
				b=road.mNodes.get(1).mLocation; 
				addInfobulles(ctx, road);
			} else{
//				DrivingActivity.trajet.road=road;
			}
			bb=new BoundingBoxE6(a.getLatitudeE6(), a.getLongitudeE6(),b.getLatitudeE6(), b.getLongitudeE6());
			GeoPoint center = new GeoPoint((a.getLatitudeE6()+b.getLatitudeE6())/2, (b.getLongitudeE6()+a.getLongitudeE6())/2);

			mapView.getController().zoomToSpan(bb);
			mapView.getController().setCenter(center);

			if(!navigation){
				//mapView.getController().zoomOut();
			}

			if(dialog!=null){
				dialog.dismiss();
			}
			try{
				((SherlockActivity) ctx).setSupportProgressBarIndeterminateVisibility(false);
			}catch(ClassCastException e){
				//pas grave
			}
			mapView.invalidate();


		} else{
			if(dialog!=null){
				dialog.dismiss();
			}
			try{
				((SherlockActivity) ctx).setSupportProgressBarIndeterminateVisibility(false);
			}catch(ClassCastException e){
				//pas grave
			}
			
			Toast.makeText(ctx,"check your internet connection", 4000).show();
		}

		//fin.run();
		
	}


	private void addInfobulles(Context ctx, Road road) {

		final ArrayList<ExtendedOverlayItem> roadItems = new ArrayList<ExtendedOverlayItem>();
		ItemizedOverlayWithBubble<ExtendedOverlayItem> roadNodes = new ItemizedOverlayWithBubble<ExtendedOverlayItem>(ctx, roadItems, mapView);
		mapView.getOverlays().add(roadNodes);

		Drawable marker = ctx.getResources().getDrawable(R.drawable.marker_node);
		for (int i=0; i<road.mNodes.size(); i++){
			RoadNode node = road.mNodes.get(i);
			String insts=StringEscapeUtils.unescapeHtml4(node.mInstructions);
			ExtendedOverlayItem nodeMarker = new ExtendedOverlayItem(insts, "", node.mLocation, ctx);
			nodeMarker.setMarkerHotspot(OverlayItem.HotspotPlace.CENTER);
			nodeMarker.setMarker(marker);
			nodeMarker.setDescription(road.getLengthDurationText(node.mLength, node.mDuration));
			//nodeMarker.setSubDescription();
			Drawable icon = ctx.getResources().getDrawable(R.drawable.ic_continue);
			nodeMarker.setImage(icon);
			roadNodes.addItem(nodeMarker);
			
			if(i==0)
			((TextView) ((Activity) ctx).findViewById(R.id.inst)).setText(insts);

		}
		


	}

	public Overlay getOverlay() {
		return roadOverlay;
	}

	public void setDisplayed(boolean b) {
		if(!b)
			roadOverlay=null;
	}


}