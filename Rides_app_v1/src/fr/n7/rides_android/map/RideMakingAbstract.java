package fr.n7.rides_android.map;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.osmdroid.tileprovider.util.CloudmadeUtil;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MyLocationOverlay;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.actionbarsherlock.view.Window;

import fr.n7.rides_android.R;
import fr.n7.rides_android.RidesActivity;



/**
 * Elle geolocalise l'utilisateur et lui permet de choisir une destination
 * Utile pour proposer et chercher
 * @author J
 *
 */
public abstract class RideMakingAbstract extends RidesActivity  {
	protected MapView mapView;
	protected Button validation;

	public OverlayRide overlayWaypoints;
	public MyLocationOverlay locationOverlay;
	private ArrayAdapterAddresses addressAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setTheme(com.actionbarsherlock.R.style.Theme_Sherlock);


		setContentView(R.layout.activity_choix_destination);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		CloudmadeUtil.retrieveCloudmadeKey(this);

		mapView= (MapView) findViewById(R.id.mapview);
		validation = (Button) findViewById(R.id.button1);
		mapView.setTileSource(new CloudmadeTileSource());
		mapView.setMultiTouchControls(true);
		mapView.setBuiltInZoomControls(true);


		//place un marqueur sur la position de l'utilisateur
		affichePosition();

	}


	/**
	 * Recupere et affiche la position de l'utilisateur
	 */
	private void affichePosition(){

		locationOverlay = new MyLocationOverlay(this, mapView){
			@Override
			public void onLocationChanged(Location l) {
				super.onLocationChanged(l);
				//server.sendUpdatePosition((int) (l.getLatitude()*1E6), (int) (l.getLongitude()*1E6));
			}
		};
		locationOverlay.enableMyLocation();

		mapView.getOverlays().add(locationOverlay);
		mapView.getController().setZoom(13);

		final ProgressDialog d=ProgressDialog.show(this, "","Loading. Please wait...", true);


		final Handler handler = new Handler();
		locationOverlay.runOnFirstFix(new Runnable() { 
			public void run() {
				handler.post(new Runnable() { 
					public void run() {
						mapView.getController().setCenter(locationOverlay.getMyLocation());
						mapView.postInvalidate();
						d.dismiss();
					}
				});
			}
		});	    
	}

	@Override
	protected void onPause() {
		locationOverlay.disableMyLocation();
		super.onPause();
	}

	@Override
	protected void onResume() {
		locationOverlay.enableMyLocation();
		super.onResume();
	}


	/**
	 * Retrouve les coordonnees d'une adresse tapee
	 * @param adresse
	 */
	private void geocodeDestination(final String adresse) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select the good one");

		final ListView addressList = new ListView(this);

		addressAdapter = new ArrayAdapterAddresses(this, android.R.layout.simple_list_item_1,new ArrayList<Address>());
		addressList.setAdapter(addressAdapter);
		builder.setView(addressList);
		final Dialog dialog = builder.create();
		dialog.show();


		new AsyncTask<Void, Void, ArrayList<Address>>() {

			@Override
			protected ArrayList<Address> doInBackground(Void... params) {
				ArrayList<Address> liste=new ArrayList<Address>();
				try {
					final Geocoder geoCoder = new Geocoder(RideMakingAbstract.this, Locale.getDefault());
					liste = (ArrayList<Address>) geoCoder.getFromLocationName(adresse, 5);
				} catch (Exception e) {
					Toast.makeText(RideMakingAbstract.this, "Can't find this address", Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}

				return liste;
			}

			protected void onPostExecute(ArrayList<Address> result) {
				addressAdapter.clear();
				addressAdapter.addAll(result);
			};
		}.execute();

		addressList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if(dialog!=null){
					dialog.dismiss();
				}
				GeoPoint nouveauGP=new GeoPoint((int) (addressAdapter.getItem(arg2).getLatitude() * 1E6),
						(int) (addressAdapter.getItem(arg2).getLongitude() * 1E6));
				overlayWaypoints.ajouterDestination(nouveauGP);
				mapView.getController().setCenter(nouveauGP);
			}
		});
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		final MenuItem search=menu.add("Search");
		search.setIcon(R.drawable.ic_search)
		.setActionView(R.layout.collapsible_edittext)
		.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);

		final EditText view=((EditText) search.getActionView());

		search.setOnMenuItemClickListener(new OnMenuItemClickListener() {

			@Override
			public boolean onMenuItemClick(MenuItem item) {
				view.requestFocus();
				view.postDelayed(new Runnable() {
					@Override
					public void run() {
						InputMethodManager keyboard = (InputMethodManager)
								getSystemService(Context.INPUT_METHOD_SERVICE);
						keyboard.showSoftInput(view, 0);
					}
				},200);
				return false;
			}
		});

		view.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if(keyCode==KeyEvent.KEYCODE_ENTER){
					geocodeDestination(view.getText().toString());
					search.collapseActionView();
					InputMethodManager inputMethodManager = (InputMethodManager)  RideMakingAbstract.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
					inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
					mapView.requestFocus();
				}
				return false;
			}
		});


		return true;
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==-1){
			setResult(-1);
			finish();
		}
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId()==android.R.id.home){
			setResult(-1);
			finish();
		}
		return true;
	}



	class ArrayAdapterAddresses extends ArrayAdapter<Address>{

		public ArrayAdapterAddresses(Context context, int textViewResourceId, List<Address> list) {
			super(context, textViewResourceId,list);
		}


		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) LayoutInflater.from( this.getContext() );
				v = vi.inflate(android.R.layout.simple_list_item_1, null);
			}

			Address a = this.getItem(position);
			if (a != null) {
				TextView tt = (TextView) v.findViewById(android.R.id.text1);
				if (tt != null) {
					String s="";
					if(a.getMaxAddressLineIndex()>1){
						s=a.getAddressLine(0)+", "+a.getAddressLine(1);
					} else if(a.getFeatureName()!=null){
						s=a.getFeatureName();
					} else{
						s=a.getLocality();
					}
					tt.setText(s);
				}
			}
			return v;
		}


	}
}
