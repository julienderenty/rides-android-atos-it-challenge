package fr.n7.rides_android.driver;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.OverlayItem;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import fr.n7.rides_android.R;
import fr.n7.rides_android.map.ItemizedOverlayDraggable;
import fr.n7.rides_android.map.RideMakingAbstract;

public class RideMakingActivity extends RideMakingAbstract {

	/* MyListener class */
	class MyListener implements OnTouchListener, OnClickListener {
		public boolean onTouch(View v, MotionEvent e) {
			if (e.getAction()==e.ACTION_MOVE) {
				Log.d("iv","ACTION MOVE"); // now it is called
			} else if (e.getAction()==e.ACTION_DOWN) {
				Log.d("iv","ACTION_DOWN"); // called
			}
			return false;
		}

		public void onClick(View v) {
		}
	}

	public  static boolean flag;		
	


	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final Drawable d=getResources().getDrawable(R.drawable.marker_destination);
		final ImageView drag=(ImageView) findViewById(R.id.drag);
		final DefaultResourceProxyImpl r = new DefaultResourceProxyImpl(RideMakingActivity.this);
		final ItemizedOverlayDraggable io;
		io=new ItemizedOverlayDraggable(d, r, drag, RideMakingActivity.this, null);
		mapView.getOverlays().add(io);
		
		final ImageView iv= (ImageView) findViewById(R.id.imageView1);
		
		OnClickListener myListener = new MyListener();
		iv.setOnClickListener(myListener);
		iv.setOnTouchListener(new MyListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				 flag = true;
				if(event.getAction()==event.ACTION_DOWN){
					GeoPoint p=ItemizedOverlayDraggable.geoPointFromScreenCoords(
							(int) event.getX()+iv.getLeft(), (int) event.getY()+iv.getTop(), mapView);
					io.addOverlay(new OverlayItem("", "", p));
					mapView.invalidate();
				}
				io.onTouchEvent(event, mapView);
				flag=false;
				return false;
			}
		}
		);

		validation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (DrivingActivity.trajet.waypoints==null ||DrivingActivity.trajet.waypoints.size()<1){
					Toast.makeText(RideMakingActivity.this, "You haven't choosen a destination" , Toast.LENGTH_SHORT).show();
				} else if(!overlayWaypoints.task.displayed()){
					Toast.makeText(RideMakingActivity.this, "Road couldn't be calculated yet.  Let's try again..." , 10000).show();
					((OverlayRideMaking) overlayWaypoints).displayRide();						
				}
				else{



					/**
					 * Geocoding reverse to name the ride with the arrival address 
					 */
					GeoPoint destination=DrivingActivity.trajet.waypoints.get(DrivingActivity.trajet.waypoints.size()-1);
					double lat=((double) destination.getLatitudeE6())/1000000;
					double lon= ((double) destination.getLongitudeE6())/1000000;

					Geocoder geoCoder = new Geocoder(RideMakingActivity.this, Locale.getDefault());
					List<Address> res = null;
					try {
						res = geoCoder.getFromLocation(lat,lon,1);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(res!=null && res.size()>0){
						DrivingActivity.trajet.titre= res.get(0).getThoroughfare()+ ", "+res.get(0).getLocality();
					} else{
						DrivingActivity.trajet.titre=destination.toString();

					}

					/**
					 * Start driving !
					 */
					Intent intent = new Intent(RideMakingActivity.this,DrivingActivity.class);
					startActivityForResult(intent, 0);
				}
			}
		});


		overlayWaypoints=new OverlayRideMaking(this,mapView);
		mapView.getOverlays().add(overlayWaypoints);	

	}


	private MenuItem clear;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		clear=menu.add( "Clear")
				.setIcon(R.drawable.abs__ic_clear_normal);
		clear.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getTitle().equals("Clear")){
			((OverlayRideMaking) super.overlayWaypoints).raz();
			mapView.invalidate();
		}
		return super.onOptionsItemSelected(item);
	}

}