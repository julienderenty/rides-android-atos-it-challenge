package fr.n7.rides_android.driver;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.view.MenuItem;

import fr.n7.rides_android.R;
import fr.n7.rides_android.Ride;
import fr.n7.rides_android.RidesActivity;

public class RideDescribingActivity extends RidesActivity {
	
	int nbPlaces=0;
	int volumeCoffre=0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//        setTheme(com.actionbarsherlock.R.style.Theme_Sherlock);

		//Ajout bouton precedent
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_desc_trajet_conducteur);
		
		final Context context = this;
		
		Spinner s = (Spinner) findViewById(R.id.spinnerVolumeCoffre);
		final SeekBar sb = (SeekBar) findViewById(R.id.seekBarNbPlacesAssises);
		
		s.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				volumeCoffre=arg2;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		sb.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				nbPlaces=seekBar.getProgress();	
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				((TextView) findViewById(R.id.nbPlacesAssises)).setText(seekBar.getProgress()+"");
				
			}
		});
		sb.setProgress(3);
		s.setSelection(3);

		
		
		Button b= (Button) findViewById(R.id.button1);
		b.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context,RideMakingActivity.class);
				intent.putExtra("conducteur", true);
				DrivingActivity.trajet=new Ride("");
				DrivingActivity.trajet.nbPlaces=nbPlaces;
				DrivingActivity.trajet.volumeCoffre=volumeCoffre;
				startActivityForResult(intent, 0);				
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==-1){
			setResult(-1);
			finish();
		}
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId()==android.R.id.home){
			setResult(-1);
			finish();
		}
		return true;
	}
	

}
