package fr.n7.rides_android.driver;

import java.util.ArrayList;
import java.util.List;

import org.osmdroid.bonuspack.overlays.ExtendedOverlayItem;
import org.osmdroid.bonuspack.overlays.ItemizedOverlayWithBubble;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.OverlayItem;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;

import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import fr.n7.rides_android.R;
import fr.n7.rides_android.util.Profile;

public class Passengers {

	public DrivingActivity activity;
	public List<Profile> listePassengers=new ArrayList<Profile>();
	ItemizedOverlayWithBubble<ExtendedOverlayItem> passengersOverlay;	
	

	public Passengers(DrivingActivity drivingActivity) {
		this.activity=drivingActivity;
		
		passengersOverlay = new ItemizedOverlayWithBubble<ExtendedOverlayItem>(activity, new ArrayList<ExtendedOverlayItem>(), activity.mapView);
		activity.mapView.getOverlays().add(passengersOverlay);

	}



	public void requetePassager(){
		createPinPassenger(new GeoPoint(43605691,1448736));
		activity.startActionMode(new NotifiedActionModes());
	}

	private final class NotifiedActionModes implements ActionMode.Callback {
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {

			mode.setTitle("Request");
			menu.add("Accept")
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			menu.add("Show")
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			menu.add("Refuse")
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

			beep(14);

			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			
			long askeruid = 0;
			if(item.getTitle().equals("Accept")){
				mode.finish();
				activity.server.sendChoiceFromDriver(1, askeruid);
				
			} else if(item.getTitle().equals("Show")){	
				item.setVisible(false);
				activity.myLocationOverlay.disableFollowLocation();
				activity.mapView.getController().zoomToSpan(bb);
				activity.mapView.getController().animateTo(center);
				
			} else if(item.getTitle().equals("Refuse")){
				passengersOverlay.removeItem(passengersOverlay.size()-1);
				mode.finish();
				activity.server.sendChoiceFromDriver(0, askeruid);
			}
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
		}
		
		private void beep(int volume)
		{
		    AudioManager manager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
		    manager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);

		    Uri notification = RingtoneManager
		            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		    MediaPlayer player = MediaPlayer.create(activity.getApplicationContext(), notification);
//		    player.start();
		}
	}

/****OVERLAYS****/
	
	
	GeoPoint center;
	BoundingBoxE6 bb;
	
	public void createPinPassenger(GeoPoint geopoint){
		Drawable pin=activity.getResources().getDrawable(R.drawable.marker_via);


		ExtendedOverlayItem passengerMarker = new ExtendedOverlayItem("insts", "", geopoint,activity);
		passengerMarker.setMarkerHotspot(OverlayItem.HotspotPlace.CENTER);
		passengerMarker.setMarker(pin);
		passengerMarker.setDescription("desc");
		//nodeMarker.setSubDescription();
		Drawable icon = activity.getResources().getDrawable(R.drawable.ic_continue);
		passengerMarker.setImage(icon);

		
		passengersOverlay.addItem(passengerMarker);
		passengersOverlay.showBubbleOnItem(0, activity.mapView, true);

		
		//boutons dans bulle
		//new ItemizedOverlayWithBubble<ExtendedOverlayItem>(this, roadItems, mapView,new CustomInfoWindow(mapView));
		
		
		GeoPoint a=activity.myLocationOverlay.getMyLocation();
		GeoPoint b=geopoint;
		 bb=new BoundingBoxE6(a.getLatitudeE6(), a.getLongitudeE6(),b.getLatitudeE6(), b.getLongitudeE6());
		
		center = new GeoPoint((a.getLatitudeE6()+b.getLatitudeE6())/2, (b.getLongitudeE6()+a.getLongitudeE6())/2);

	}

	/******************************CODE TEST*********************************/
	
	
//	MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.notif_default);
	//	MediaPlayer mediaPlayer=MediaPlayer.create(this, AudioManager.STREAM_NOTIFICATION);
	//mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);

	//setVolumeControlStream(AudioManager.STREAM_NOTIFICATION);
	//mediaPlayer.setVolume(0.1f,0.1f);
//	mediaPlayer.start();
	
//	public AlertDialog creerDialog(final Context ctx){
//		
//
//	// 1. Instantiate an AlertDialog.Builder with its constructor
//	AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
//
//	// 2. Chain together various setter methods to set the dialog characteristics
//	builder.setMessage("Demande")
//	       .setTitle("Passager")
//	       .setNegativeButton("Refuser", new OnClickListener() {
//			
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				new ServerImpl().sendReponseConducteur(DrivingActivity.trajet.id,false);
//				activity.myLocationOverlay.enableFollowLocation();
//			}
//		})
//		.setNeutralButton("Carte", new OnClickListener() {
//			
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				(((DrivingActivity) ctx).passengers).createPinPassenger(new GeoPoint(47.642203,1.650306));
//				activity.myLocationOverlay.disableFollowLocation();
//			}
//		})
//		.setPositiveButton("Accepter", new OnClickListener() {
//			
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				new ServerImpl().sendReponseConducteur(DrivingActivity.trajet.id,true);
//				activity.myLocationOverlay.enableFollowLocation();
//			}
//		});
//
//	// 3. Get the AlertDialog from create()
//	return builder.create();
//	}

}
