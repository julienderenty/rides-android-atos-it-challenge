package fr.n7.rides_android.driver;

import java.util.ArrayList;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.PathOverlay;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import fr.n7.rides_android.R;
import fr.n7.rides_android.map.DisplayRoadTask;
import fr.n7.rides_android.map.ItemizedOverlayDraggable;
import fr.n7.rides_android.map.OverlayRide;

public class OverlayRideMaking extends OverlayRide{
	RideMakingActivity activity;
	protected ItemizedOverlayDraggable overlayDest;
	private ItemizedOverlayDraggable overlayWaypoints;
	private boolean depart=false;

	public OverlayRideMaking(RideMakingActivity activity, MapView mapView) {
		super(activity,mapView);
		this.activity=activity;


		overlayDest=new ItemizedOverlayDraggable(drawable, new DefaultResourceProxyImpl(activity), dragImage,activity, new Runnable() {
			@Override
			public void run() {
				displayRide();
			}
		} );

		overlayWaypoints=new ItemizedOverlayDraggable(activity.getResources().getDrawable(R.drawable.marker_node),
				new DefaultResourceProxyImpl(activity), dragImageWaypoint,activity, new Runnable() {
			@Override
			public void run() {
				displayRide();
			}
		} );
		mapView.getOverlays().add(overlayDest);
		mapView.getOverlays().add(overlayWaypoints);

	}

	@Override
	protected void draw(Canvas arg0, MapView arg1, boolean arg2) {
	}

	@Override
	public boolean onLongPress(MotionEvent e, MapView mapView) {
		GeoPoint nouveauGP = (GeoPoint) mapView.getProjection().fromPixels((int) e.getX(), (int) e.getY()) ;
		ajouterDestination(nouveauGP);
		mapView.invalidate();
		return true;
	}

	public void ajouterDestination(GeoPoint p){
		DrivingActivity.trajet.waypoints.add(p);
		displayRide();
	}

	
	public void raz(){
		DrivingActivity.trajet.waypoints.clear();		
		overlayDest.clear();
		overlayWaypoints.clear();
		mapView.getOverlays().remove(task.getOverlay());
		mapView.invalidate();
		depart=false;
		task.setDisplayed(false);
	}

	public void displayRide(){
		mapView.getOverlays().remove(task.getOverlay());

		activity.setSupportProgressBarIndeterminateVisibility(true);

		overlayDest.clear();
		overlayWaypoints.clear();
		
		ArrayList<GeoPoint> wp=DrivingActivity.trajet.waypoints;
		
		if(!depart){
			wp.add(0,activity.locationOverlay.getMyLocation());
			depart=true;
		}else{
			wp.set(0,activity.locationOverlay.getMyLocation());
		}
		

		if(wp.size()>1){
			wp=tri(wp);


			for(int i=1;i<wp.size()-1;i++){
				GeoPoint p=wp.get(i);
				overlayWaypoints.addOverlay(new OverlayItem("", "", p));
			}
			
			overlayDest.addOverlay(new OverlayItem("", "", wp.get(wp.size()-1)));
		}

		DrivingActivity.trajet.waypoints=wp;
		task=new DisplayRoadTask(mapView, activity, false, null);
		task.execute(DrivingActivity.trajet.waypoints);
		
	
}


private ArrayList<GeoPoint> tri(ArrayList<GeoPoint> waypoints) {
	if(waypoints.size()<2){
		return waypoints;
	}

	ArrayList<GeoPoint> res= new ArrayList<GeoPoint>();
	res.add(waypoints.get(0));

	for(int i=0;i<waypoints.size()-1;i++){
		ArrayList<GeoPoint> reste=(ArrayList<GeoPoint>) waypoints.clone();
		reste.removeAll(res);
		if(reste.size()>0){
			GeoPoint courant=res.get(i);
			int min=0;
			for(GeoPoint pointReste: reste){
				if(pointReste.distanceTo(courant)<reste.get(min).distanceTo(courant)){
					min=reste.indexOf(pointReste);
				}
			}
			res.add(reste.get(min));
		}
	}

	return res;
}
}

