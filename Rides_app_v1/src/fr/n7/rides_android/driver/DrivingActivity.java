package fr.n7.rides_android.driver;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.osmdroid.tileprovider.util.CloudmadeUtil;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MyLocationOverlay;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;

import com.actionbarsherlock.view.MenuItem;

import fr.n7.rides_android.R;
import fr.n7.rides_android.Ride;
import fr.n7.rides_android.RidesActivity;
import fr.n7.rides_android.comm.util.remote.TalkToServiceImpl;
import fr.n7.rides_android.comm.util.remote.TalkToServiceImpl.latlon;
import fr.n7.rides_android.map.CloudmadeTileSource;
import fr.n7.rides_android.map.DisplayRoadTask;


/**
 * Elle geolocalise l'utilisateur et lui permet de choisir une destination
 * Utile pour proposer et chercher
 * @author J
 *
 */
public class DrivingActivity extends RidesActivity  {

	public MapView mapView;
	public MyLocationOverlay myLocationOverlay;

	public static Ride trajet;

	public Passengers passengers;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_faire_trajet);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		CloudmadeUtil.retrieveCloudmadeKey(this);


		mapView=(MapView) findViewById(R.id.mapview);
		mapView.setTileSource(new CloudmadeTileSource());

		mapView.setBuiltInZoomControls(false);
		mapView.setMultiTouchControls(true);

		affichePosition();

		new DisplayRoadTask(mapView,this, true,null).execute(DrivingActivity.trajet.waypoints);

		int originLat=trajet.waypoints.get(0).getLatitudeE6();
		int originLon=trajet.waypoints.get(0).getLatitudeE6();
		int destLat=trajet.waypoints.get(trajet.waypoints.size()-1).getLatitudeE6();
		int destLon=trajet.waypoints.get(trajet.waypoints.size()-1).getLongitudeE6();
		int nbPlaces=trajet.nbPlaces;
		Date arrivalDate=new Date();
		List<latlon> waypoints=new ArrayList<TalkToServiceImpl.latlon>();
		server.sendStoreRide(originLat, originLon, destLat, destLon, nbPlaces, arrivalDate, waypoints);

		passengers=new Passengers(this);

		// SLEEP 2 SECONDS HERE ...
		Handler handler = new Handler(); 
		handler.postDelayed(new Runnable() { 
			public void run() { 
				passengers.requetePassager();
			} 
		}, 7000); 
	}





	/**
	 * Recupere et affiche la position de l'utilisateur
	 */
	private void affichePosition(){

		myLocationOverlay=new MyLocationOverlay(this, mapView){
			@Override
			public void onLocationChanged(Location arg0) {
				// TODO Auto-generated method stub
				super.onLocationChanged(arg0);
			}
		};
		
		myLocationOverlay.enableMyLocation();
		myLocationOverlay.enableCompass();
		myLocationOverlay.enableFollowLocation();

		mapView.getOverlays().add(myLocationOverlay);
		mapView.getController().setZoom(18);

		final ProgressDialog d=ProgressDialog.show(this, "","Loading. Please wait...", true);

		final Handler handler = new Handler();
		myLocationOverlay.runOnFirstFix(new Runnable() { 
			public void run() {
				handler.post(new Runnable() { 
					public void run() {
						mapView.getController().setCenter(myLocationOverlay.getMyLocation());
						mapView.postInvalidate();
						d.dismiss();
					}
				});
			}
		});

	}

	@Override
	protected void onPause() {
		myLocationOverlay.disableMyLocation();
		myLocationOverlay.disableCompass();
		super.onPause();
	}

	@Override
	protected void onResume() {
		myLocationOverlay.enableMyLocation();
		myLocationOverlay.enableCompass();
		super.onResume();
	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==-1){
			setResult(-1);
			finish();
		}
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId()==android.R.id.home){
			setResult(-1);
			finish();
		}
		return true;
	}



}
