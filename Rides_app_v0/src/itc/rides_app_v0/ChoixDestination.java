package itc.rides_app_v0;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;
/**
 * Elle g�olocalise l'utilisateur et lui permet de choisir une destination
 * Utile pour proposer et chercher
 * @author J
 *
 */
public class ChoixDestination extends MapActivity {
	MapView mapView;
	int[] loc;
	GeoPoint destGeoPoint;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choix_destination);

		//Ajout bouton pr�c�dent
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		//Initialisation map
		mapView = (MapView) findViewById(R.id.mapview);
		mapView.getController().setZoom(16);
		
		//place un marqueur sur la position de l'utilisateur
		affichePosition();

		//champ de saisie de la destination 
		final EditText edit=(EditText) findViewById(R.id.editText1);

		//� l'appui sur Enter, la map se centre sur la destination saisie        
		edit.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if(keyCode==KeyEvent.KEYCODE_ENTER){
					mapView.getOverlays().clear();
					geocode(edit.getText().toString());
				}
				return false;
			}
		});


		//r�cup�re infos sur l'activit� pr�c�dente pour savoir si on est un conducteur ou un passager
		final boolean conducteur=this.getIntent().getBooleanExtra("conducteur", false); 
		Log.e("conducteur ?",conducteur+"");
		final Context context= this;
		final Button validation=(Button) findViewById(R.id.button1);
		validation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(conducteur){
					if(((Button) findViewById(R.id.button1)).getText().equals("Valider l'itin�raire")){
						Intent intent = new Intent(context,DescTrajet.class);
						context.startActivity(intent);
						((Activity) context).finish();
					}else{
						Log.e("****", "it");
						new ItiTask().execute();
						validation.setText("Valider l'itin�raire");
					}
				} else{
					listeResultats();
				}

			}
		});

	}






	private void listeResultats(){
		//		Bundle bundle=getIntent().getExtras();
		//		Intent intent = new Intent(context,ResultatsRecherche.class);
		//		context.startActivity(intent, bundle);
		//		((Activity) context).finish();
	}
	/**
	 * Retrouve les coordonn�es d'une adresse tap�e
	 * @param adresse
	 */
	private void geocode(String adresse) {
		Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
		try {
			List<Address> addresses = geoCoder.getFromLocationName(adresse, 5);
			String strCompleteAddress = "";
			if (addresses.size() > 0) {
				destGeoPoint = new GeoPoint(
						(int) (addresses.get(0).getLatitude() * 1E6),
						(int) (addresses.get(0).getLongitude() * 1E6));
				mapView.getController().animateTo(destGeoPoint);
				mapView.invalidate();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	/**
	 * R�cup�re et affiche la position de l'utilisateur
	 */
	private void affichePosition(){
				
		List overlays = mapView.getOverlays();
	    final MyLocationOverlay myLocationOverlay = new MyLocationOverlay(this, mapView);
	    myLocationOverlay.enableMyLocation();
	    overlays.add(myLocationOverlay);
	    myLocationOverlay.runOnFirstFix(new Runnable() { public void run() {
            mapView.getController().animateTo(myLocationOverlay.getMyLocation());
        }});
	    
	}

	/**
	 * R�cup�re la position de l'utilisateur
	 * @return
	 */
	private int[] getGPS() {
		LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);  
		List<String> providers = lm.getProviders(true);

		/* Loop over the array backwards, and if you get an accurate location, then break                 out the loop*/
		Location l = null;

		for (int i=providers.size()-1; i>=0; i--) {
			l = lm.getLastKnownLocation(providers.get(i));
			if (l != null) break;
		}

		int[] gps = new int[2];
		if (l != null) {
			gps[0] = (int) Math.floor(l.getLatitude()*1000000);
			gps[1] =(int) Math.floor(l.getLongitude()*1000000);
		}
		return gps;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_choix_destination, menu);
		return true;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	
	/**
	 * Permet la r�cup�ration d'un iti en background puis affichage sur la carte
	 * @author J
	 *
	 */
	private class ItiTask extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void...dest) {
			GeoPoint srcGeoPoint = new GeoPoint(loc[0],loc[1]);
			DrawPath(srcGeoPoint, destGeoPoint, Color.GREEN, mapView);
			mapView.getController().animateTo(srcGeoPoint);
			mapView.getController().setZoom(13);
			return null;
		}



		private void DrawPath(GeoPoint src, GeoPoint dest, int color,
				MapView mMapView) {
			// connect to map web service
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(makeUrl(src, dest));
			HttpResponse response;
			try {
				response = httpclient.execute(httppost);

				HttpEntity entity = response.getEntity();
				InputStream is = null;

				is = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(
						is, "iso-8859-1"), 8);
				StringBuilder sb = new StringBuilder();
				sb.append(reader.readLine() + "\n");
				String line = "0";
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				reader.close();
				String result = sb.toString();
				JSONObject jsonObject = new JSONObject(result);
				JSONArray routeArray = jsonObject.getJSONArray("routes");
				JSONObject routes = routeArray.getJSONObject(0);
				JSONObject overviewPolylines = routes
						.getJSONObject("overview_polyline");
				String encodedString = overviewPolylines.getString("points");
				List<GeoPoint> pointToDraw = decodePoly(encodedString);
				mMapView.getOverlays().add(new MyOverLay(pointToDraw));
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}

		}

		private List<GeoPoint> decodePoly(String encoded) {

			List<GeoPoint> poly = new ArrayList<GeoPoint>();
			int index = 0, len = encoded.length();
			int lat = 0, lng = 0;

			while (index < len) {
				int b, shift = 0, result = 0;
				do {
					b = encoded.charAt(index++) - 63;
					result |= (b & 0x1f) << shift;
					shift += 5;
				} while (b >= 0x20);
				int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
				lat += dlat;

				shift = 0;
				result = 0;
				do {
					b = encoded.charAt(index++) - 63;
					result |= (b & 0x1f) << shift;
					shift += 5;
				} while (b >= 0x20);
				int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
				lng += dlng;

				GeoPoint p = new GeoPoint((int) (((double) lat / 1E5) * 1E6),
						(int) (((double) lng / 1E5) * 1E6));
				poly.add(p);
			}

			return poly;
		}

		private String makeUrl(GeoPoint src, GeoPoint dest) {
			// TODO Auto-generated method stub

			StringBuilder urlString = new StringBuilder();

			urlString.append("http://maps.googleapis.com/maps/api/directions/json");
			urlString.append("?origin=");// from
			urlString.append(Double.toString((double) src.getLatitudeE6() / 1.0E6));
			urlString.append(",");
			urlString
			.append(Double.toString((double) src.getLongitudeE6() / 1.0E6));
			urlString.append("&destination=");// to
			urlString
			.append(Double.toString((double) dest.getLatitudeE6() / 1.0E6));
			urlString.append(",");
			urlString
			.append(Double.toString((double) dest.getLongitudeE6() / 1.0E6));
			urlString.append("&sensor=false");

			Log.d("xxx", "URL=" + urlString.toString());
			return urlString.toString();
		}

		class MyOverLay extends Overlay {
			private int pathColor;
			private final List<GeoPoint> points;
			private boolean drawStartEnd;

			public MyOverLay(List<GeoPoint> pointToDraw) {
				// TODO Auto-generated constructor stub
				this(pointToDraw, Color.GREEN, true);
			}

			public MyOverLay(List<GeoPoint> points, int pathColor,
					boolean drawStartEnd) {
				this.points = points;
				this.pathColor = pathColor;
				this.drawStartEnd = drawStartEnd;
			}

			private void drawOval(Canvas canvas, Paint paint, Point point) {
				Paint ovalPaint = new Paint(paint);
				ovalPaint.setStyle(Paint.Style.FILL_AND_STROKE);
				ovalPaint.setStrokeWidth(2);
				ovalPaint.setColor(Color.BLUE);
				int _radius = 6;
				RectF oval = new RectF(point.x - _radius, point.y - _radius,
						point.x + _radius, point.y + _radius);
				canvas.drawOval(oval, ovalPaint);
			}

			public boolean draw(Canvas canvas, MapView mapView, boolean shadow,
					long when) {
				Projection projection = mapView.getProjection();
				if (shadow == false && points != null) {
					Point startPoint = null, endPoint = null;
					Path path = new Path();
					// We are creating the path
					for (int i = 0; i < points.size(); i++) {
						GeoPoint gPointA = points.get(i);
						Point pointA = new Point();
						projection.toPixels(gPointA, pointA);
						if (i == 0) { // This is the start point
							startPoint = pointA;
							path.moveTo(pointA.x, pointA.y);
						} else {
							if (i == points.size() - 1)// This is the end point
								endPoint = pointA;
							path.lineTo(pointA.x, pointA.y);
						}
					}

					Paint paint = new Paint();
					paint.setAntiAlias(true);
					paint.setColor(pathColor);
					paint.setStyle(Paint.Style.STROKE);
					paint.setStrokeWidth(5);
					paint.setAlpha(90);
					if (getDrawStartEnd()) {
						if (startPoint != null) {
							drawOval(canvas, paint, startPoint);
						}
						if (endPoint != null) {
							drawOval(canvas, paint, endPoint);
						}
					}
					if (!path.isEmpty())
						canvas.drawPath(path, paint);
				}
				return super.draw(canvas, mapView, shadow, when);
			}

			public boolean getDrawStartEnd() {
				return drawStartEnd;
			}

			public void setDrawStartEnd(boolean markStartEnd) {
				drawStartEnd = markStartEnd;
			}
		}

	}
}