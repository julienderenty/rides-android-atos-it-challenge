package itc.rides_app_v0;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import itc.rides_app_v0.ChoixDestination;

public class Main extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choix_type);
		
		//Bouton chercher lance l'activity choix destination
        Button chercher=(Button) findViewById(R.id.button1);
        final Context context=this;
        chercher.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Intent intent = new Intent(context,ChoixDestination.class);
				context.startActivity(intent);
				((Activity) context).finish();
			}
		});
        

      //Bouton proposer aussi
        Button proposer=(Button) findViewById(R.id.button2);
        proposer.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {

				Intent intent = new Intent(context,ChoixDestination.class);
				intent.putExtra("conducteur", true);
				context.startActivity(intent);
				((Activity) context).finish();
			}
		});
        
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_choix_type, menu);
		return true;
	}

}
