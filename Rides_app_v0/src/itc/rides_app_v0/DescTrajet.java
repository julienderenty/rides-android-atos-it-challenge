package itc.rides_app_v0;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class DescTrajet extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Ajout bouton précédent
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_desc_trajet);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_desc_trajet, menu);
		return true;
	}

}
